package webconsole

import (
	"os"

	"github.com/gopherjs/gopherjs/js"
)

var console = os.Stdout

func init() {
	// Don't run in Node
	if process := js.Global.Get("process"); process != js.Undefined {
		return
	}
}

func Enable() {

}

func Disable() {
	os.Stdout = console
}
