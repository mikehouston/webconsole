var last = Date.now();
var clog = console.log;
var cerr = console.error;

var log = "";

function documentlog(err, args) {
    var now = Date.now();
    var body = document.getElementsByTagName("body")[0]
    var row = '<pre style="margin: 5px 0 0 5em; border-left: 1px solid #CCC; padding-left: 5px;">';
    
    var delta = (now - last);
    if (delta > 0) {
        row += '<span style="position:absolute; left:5px; width: 4.5em; text-align: right; color: red;">+' + delta + 'ms</span>';
    }
    last = now;
    
    if (err) {
        row += '<span style="color:red;">'
    }
    for (var i = 0; i < args.length; i++) {
        if (i > 0) {
            row += " ";
        }
        if (typeof args[i] === "string") {
            row += args[i];
        } else if (args[i].$array) {
            row += JSON.stringify(args[i].$array);
        } else if (args[i].$val && args[i].Object) {
            row += JSON.stringify(args[i].Object);
        } else {
            row += args[i];
        }
    }
    if (err) {
        row += '</span>'
    }
    row += "&nbsp;</pre>";

    if (body != null) {
        if (log != null) {
            body.innerHTML += log;
            log = null;
        }
        body.innerHTML += row;
    } else {
        log += row;
    }
}

function dlog() {
    documentlog(false, arguments);
    clog.apply(null, arguments);
}

function derr() {
    documentlog(true, arguments);
    cerr.apply(null, arguments);
}

if (typeof window !== 'undefined') {
    window.console.log = dlog;
    window.console.error = derr;
}
